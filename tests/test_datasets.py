# PyTorch Datasets utility repository
# Copyright (C) 2020-2023  Abien Fred Agarap
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Unit test for data loaders"""
import numpy as np

from pt_datasets import create_dataloader, load_dataset


def test_load_mnist():
    train_data, test_data = load_dataset("mnist")
    assert (len(train_data), len(test_data)) == (60_000, 10_000)
    assert train_data.data.numpy().min() == 0
    assert train_data.data.numpy().max() == 255
    assert test_data.data.numpy().min() == 0
    assert test_data.data.numpy().max() == 255
    train_loader = create_dataloader(train_data, batch_size=32)
    for index, (batch_features, batch_labels) in enumerate(train_loader):
        if index == 1:
            break
    assert batch_features.shape == (32, 1, 28, 28)
    assert batch_labels.shape == (32,)


def test_load_fashion_mnist():
    train_data, test_data = load_dataset("fashion_mnist")
    assert (len(train_data), len(test_data)) == (60_000, 10_000)
    assert train_data.data.numpy().min() == 0
    assert train_data.data.numpy().max() == 255
    assert test_data.data.numpy().min() == 0
    assert test_data.data.numpy().max() == 255
    train_loader = create_dataloader(train_data, batch_size=32)
    for index, (batch_features, batch_labels) in enumerate(train_loader):
        if index == 1:
            break
    assert batch_features.shape == (32, 1, 28, 28)
    assert batch_labels.shape == (32,)
    assert batch_features.numpy().min() == 0.0
    assert batch_features.numpy().max() == 1.0


def test_emnist():
    train_data, test_data = load_dataset("emnist")
    assert (len(train_data), len(test_data)) == (112_800, 18_800)
    assert train_data.data.numpy().min() == 0
    assert train_data.data.numpy().max() == 255
    assert test_data.data.numpy().min() == 0
    assert test_data.data.numpy().max() == 255
    train_loader = create_dataloader(train_data, batch_size=32)
    for index, (batch_features, batch_labels) in enumerate(train_loader):
        if index == 1:
            break
    assert batch_features.shape == (32, 1, 28, 28)
    assert batch_labels.shape == (32,)


def test_cifar10():
    train_data, test_data = load_dataset("cifar10")
    assert (len(train_data), len(test_data)) == (50_000, 10_000)
    assert train_data.data.min() == 0
    assert train_data.data.max() == 255
    assert test_data.data.min() == 0
    assert test_data.data.max() == 255
    train_loader = create_dataloader(train_data, batch_size=32)
    for index, (batch_features, batch_labels) in enumerate(train_loader):
        if index == 1:
            break
    assert batch_features.shape == (32, 3, 32, 32)
    assert batch_labels.shape == (32,)
