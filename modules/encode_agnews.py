"""Module for encoding AG News using GloVe Magnitude"""
import os
import sys
import time
from pathlib import Path
from typing import Any, List, Tuple

import numpy as np

from pt_datasets.text import GloVeEncoder
from pt_datasets.utils import preprocess_data, read_data


def load_preprocessed_dataset(filename: str) -> Tuple[Any, Any]:
    path = str(Path.home())
    path = os.path.join(path, "datasets")
    path = os.path.join(path, filename)
    dataset = read_data(path)
    features, labels = (list(dataset.keys()), list(dataset.values()))
    features, labels = preprocess_data(features, labels)
    return features, labels


def encode_glove(texts: List, dim: int) -> np.ndarray:
    print("[INFO] Encoding texts...")
    encoder = GloVeEncoder(dim=dim)
    start_time = time.time()
    embeddings = encoder(texts)
    end_time = time.time()
    elapsed_time = end_time - start_time
    print(
        f"[INFO] Encoding took {(elapsed_time // 60):.0f}m {(elapsed_time % 60):.0f}s"
    )
    return embeddings


dim = int(sys.argv[1])
train_dataset = load_preprocessed_dataset("ag_news.train")
train_features, train_labels = train_dataset[0], train_dataset[1]
train_features = encode_glove(train_features, dim=dim)
train_dataset = np.concatenate((train_features, train_labels[:, None]), 1)
np.save("ag_news_train.npy", train_dataset, allow_pickle=False)

test_dataset = load_preprocessed_dataset("ag_news.test")
test_features, test_labels = test_dataset[0], test_dataset[1]
test_features = encode_glove(test_features, dim=dim)
test_dataset = np.concatenate((test_features, test_labels[:, None]), 1)
np.save("ag_news_test.npy", test_dataset, allow_pickle=False)
